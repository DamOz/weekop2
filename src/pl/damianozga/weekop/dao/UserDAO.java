package pl.damianozga.weekop.dao;

import pl.damianozga.weekop.model.User;

import java.util.List;

public interface UserDAO extends GenericDAO<User, Long> {

    List<User> getAll();
    User getUserByUsername(String username);
}

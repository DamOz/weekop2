package pl.damianozga.weekop.dao;

import pl.damianozga.weekop.model.Discovery;

import java.util.List;

public interface DiscoveryDAO extends GenericDAO<Discovery, Long> {

    List<Discovery> getAll();
}
